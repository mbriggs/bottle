#include <stdlib.h>
#include <ncurses.h>
#include <menu.h>

WINDOW *init_main_win();

int main()
{
	/* we want to tile windows, so we're not using stdscr */
	WINDOW *main_win;
	WINDOW *secondary_win;
	int key;
	int scry, scrx;

	int survivors = 1000000;

	initscr();
	cbreak();
	noecho();
	refresh();



	main_win = init_main_win();

	getmaxyx(main_win, scry, scrx);

	secondary_win = newwin(3, scrx, 0, 0);

	box(main_win, 0, 0);
	box(secondary_win, 0, 0);


	mvwprintw(main_win, scry / 2, (scrx -2) / 2 - 1, "Hello, primary!");
	mvwprintw(secondary_win, 1, 1, "survivors: %i", survivors);

	wrefresh(main_win);
	wrefresh(secondary_win);

	while ((key = getch()) != 'q') {
	};

	endwin();

	return 0;
}

WINDOW *init_main_win()
{
	int scry, scrx;
	WINDOW *new_win;

	getmaxyx(stdscr, scry, scrx);

	new_win = newwin(scry - 4, scrx - 2, 4, 0);
	wrefresh(new_win);

	return new_win;
}
